package java_project_oop;

public class InheritanceSubclass extends OPP_class_object {
	//methods created in subclass
	 public void coolingpads() {
		 System.out.println("cooler: "+Brand+" and model:" +model+ " is able to cooling air");
	 }
	

	public static void main(String[] args) {
		
		InheritanceSubclass ccooler = new InheritanceSubclass();
		ccooler.Brand = "Vargo";
		ccooler.model = " century plus";
		ccooler.coolingpads();
	}

}

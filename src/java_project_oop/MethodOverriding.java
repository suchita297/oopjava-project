package java_project_oop;

public class MethodOverriding extends OPP_class_object {
	//offbutton method of class OPP_class_object is overrriden in this
	
	public void offbutton() {
		System.out.println("cooler off after offbutton switchoff");
	}

	public static void main(String[] args) {
		
		MethodOverriding override = new MethodOverriding();
		
		override.Brand = "onida";
		override.model = "xyz"	;	
		override.offbutton();
	}

}

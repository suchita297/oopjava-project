package java_project_oop;

public class Multilevel_inheritance extends InheritanceSubclass {
	
	// attributes created in this subclass
	String rotatingfan;
	// method created in this class
	public void rotatingfan() {
	System.out.println("cooler: "+Brand+" and model:" +model+ "has" +rotatingfan+ " rotatingfan"); }

	public static void main(String[] args) {
		Multilevel_inheritance fanofcooler = new Multilevel_inheritance();
		// attributes inherits from super class
		fanofcooler.Brand = "Vargo";
		fanofcooler.model = " century plus";
		// methods inherites from superclass name as OPP_class_object
		fanofcooler.startbutton();
		fanofcooler.offbutton();
		// method inherites from subclass
		fanofcooler.coolingpads();
		// atributes created in this class is
		
		fanofcooler.rotatingfan= "Metalic";
		// method created in this subclass is used
		
		fanofcooler.rotatingfan();
	}

}
